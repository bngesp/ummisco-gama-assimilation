package ummisco.gama.assimilation.DataAdaptor;
/**
 * @author Bassirou ngom
 * create Apr 30, 2019
 * @param <T>
 */

public interface IDataSource<DataType> extends IDataAdaptor {
	
	
	public DataType[] getAllData();
	public DataType getData(int key);
	public void addData(DataType data, int key);
}
