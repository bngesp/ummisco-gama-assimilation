package ummisco.gama.assimilation.DataAdaptor;
/**
* ConnectionConstants.java
* @author Bassirou NGOM
* create May 19, 2019
*/
public class ConnectionConstants {
	public static final String CONNECTION_TYPE_MQTT ="MQTT"; 
	public static final String CONNECTION_TYPE_TCP="TCP";
	public static final String CONNECTION_TYPE_UDP="UDP";
	
	public static final String AVALAIBLE ="AVALABLE"; 
	public static final String WAIT ="WAIT"; 
	public static final String DEFAULT ="DEFAULT"; 

}
