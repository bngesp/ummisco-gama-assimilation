package ummisco.gama.assimilation.DataAdaptor;

import ummisco.gama.network.common.Connector;
import ummisco.gama.network.mqtt.MQTTConnector;
import ummisco.gama.network.tcp.TCPConnector;
import ummisco.gama.network.udp.UDPConnector;

/**
* DataAdaptor.java
* @author Bassirou NGOM
* create May 7, 2019
*/
public abstract class DataAdaptor implements IDataAdaptor{

	Connector connector;
	
	public DataAdaptor(Connector conn) {
		this.connector = conn;
	}
	
	@Override
	public Connector getDataSource() {
		return this.connector;
	}

	@Override
	public void setDataSource(Connector dataSource) {
		this.connector = dataSource;
	}

	@Override
	public abstract boolean avalaibleData();
	
	@Override
	public String getSourceConnectionType() {
		if(this.connector instanceof MQTTConnector) {
			return ConnectionConstants.CONNECTION_TYPE_MQTT;
		}if(this.connector instanceof TCPConnector) {
			return ConnectionConstants.CONNECTION_TYPE_TCP;
		}if(this.connector instanceof UDPConnector) {
			return ConnectionConstants.CONNECTION_TYPE_UDP;
		}
		return null;
	}

	
}
