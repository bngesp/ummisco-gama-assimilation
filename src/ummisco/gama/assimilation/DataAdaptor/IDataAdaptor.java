package ummisco.gama.assimilation.DataAdaptor;

import ummisco.gama.network.common.Connector;

/**
 * @author Bassirou NGOM
 *  create 27/03/2019 
 */
public interface IDataAdaptor {
	
	public Connector getDataSource();
	public void setDataSource(Connector dataSource);
	public boolean avalaibleData();
	public String getSourceConnectionType();
	
}
