package ummisco.gama.assimilation.model;

import ummisco.gama.assimilation.DataAdaptor.DataAdaptor;

public abstract class AssimilationModel {
	protected DataAdaptor data;
	protected ICorrector<?> corrector;
	protected IEstimationModel<DataAdaptor> estimationModel;
	public abstract double predict(double x);
	public abstract void addObservation(double x, double y);
	public abstract void correct();

}
