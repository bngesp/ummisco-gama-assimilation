package ummisco.gama.assimilation.model;
/**
 * 
 * @author Bassirou ngom
 * create Apr 30, 2019
 */
public interface IEstimationModel<DataType> {
	
	public void predict();
	public void predic(DataType[] v);
	public DataType[] getStateEstimation();
	public DataType[] getErrorCorrection();
	public DataType[][] getErrorCovariance();

}
