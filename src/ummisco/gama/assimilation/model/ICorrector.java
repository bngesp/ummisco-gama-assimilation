package ummisco.gama.assimilation.model;
/**
 * @author Bassirou ngom
 * create Apr 30, 2019
 */

import ummisco.gama.assimilation.DataAdaptor.IDataAdaptor;

public interface ICorrector<DataType> {
	
	public IDataAdaptor getDataAdaptor();
	public DataType[] getCurrentData();
	public DataType[] getPreviousData();
	public DataType[][] getErrorMatrix();

}
