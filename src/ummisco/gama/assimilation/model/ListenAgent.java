package ummisco.gama.assimilation.model;

import java.util.ArrayList;

public class ListenAgent {
	private ArrayList<Double> real_data;
	private ArrayList<Double> estimated_data;
	private ArrayList<Double> list_time;
	private String listenTo;
	
	
	public ListenAgent(ArrayList<Double> real_data, ArrayList<Double> estimated_data, ArrayList<Double> list_time,
			String listenTo) {
		this.real_data = real_data;
		this.estimated_data = estimated_data;
		this.list_time = list_time;
		this.listenTo = listenTo;
	}
	public ListenAgent() { this(new ArrayList<Double>(), new ArrayList<Double>(), new ArrayList<Double>(), "ALL");}
	public ListenAgent(String to) {
		this();
		this.listenTo = to;
	}
	
	public ArrayList<Double> getReal_data() {
		return real_data;
	}
	public void setReal_data(ArrayList<Double> real_data) {
		this.real_data = real_data;
	}
	public ArrayList<Double> getEstimated_data() {
		return estimated_data;
	}
	public void setEstimated_data(ArrayList<Double> estimated_data) {
		this.estimated_data = estimated_data;
	}
	public ArrayList<Double> getList_time() {
		return list_time;
	}
	public void setList_time(ArrayList<Double> list_time) {
		this.list_time = list_time;
	}
	public String getListenTo() {
		return listenTo;
	}
	public void setListenTo(String listenTo) {
		this.listenTo = listenTo;
	}
}
