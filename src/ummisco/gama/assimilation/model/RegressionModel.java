package ummisco.gama.assimilation.model;

import java.util.List;

import org.apache.commons.math3.stat.regression.RegressionResults;
import org.apache.commons.math3.stat.regression.SimpleRegression;

public class RegressionModel extends AssimilationModel{
	private List<Float> x_element;
	private List<Float> y_element;
	//private AbstractMultipleLinearRegression regression;
	private SimpleRegression regression;

	public RegressionModel() {
		regression = new SimpleRegression(true);
	}
	
	public RegressionModel(List<Float> data_sensor, List<Float> time_list) {
		this();
		this.x_element = data_sensor;
		this.y_element = time_list;
		this.initParam();
	}
	
	
	public double predict(double x){
		return this.regression.predict(x);
	}
	
	private void initParam() {
		int i=0;
		for (double d : x_element) {
			this.addObservation((double)d, (double)y_element.get(i++));
		}
	}
	
	public void addObservation(double x, double y) {
		this.regression.addData(x, y);
	}
	
	public double[] getParam() {
		RegressionResults regRes = this.regression.regress();
        double[] regResValues = regRes.getParameterEstimates();
        double intercept = regResValues[0];
        double slope = regResValues[1];
        return new double[]{intercept, slope, regRes.getRSquared()};
   
		//return .regress().getParameterEstimates();
	}
	
	public void correct() {
		
		//this.regression.
	}
	
	

}
