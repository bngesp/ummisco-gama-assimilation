package ummisco.gama.assimilation.skills;
/**
* IAssimilationSkill.java
* @author Bassirou NGOM
* create May 19, 2019
*/
public interface IAssimilationSkill {
	public static final String ASSIMILATION_CONCEPT = "ASSIMILATION";
	//***********************
	//* simple assimilation *
	//***********************
	public static final String SIMPLE_ASSIMILATION_SKILL = "simple_assimilation";
	//skill Methods
	public static final String CONNECT = "connect";
	public static final String AVAILABLE = "available";
	public static final String ESTIMATION = "estimation";
	//skill parameters
	
	//****************
	//* assimilation *
	//****************
	public static final String ASSIMILATION_SKILL  = "assimilation";
	public static String LISTEN = "listen";
	public static String LISTEN_TO = "to";
	public static String LISTEN_VAR = "var";
	public static String GET_REAL_DATA = "getRealData";
	public static String GET_ESTIMATED_DATA = "getEstimatedData";
	public static String CORRECTION_ACTION = "correction";
	public static String ESTIMATION_ACTION = "estimation";
	public static String REAL_DATA = "real_data";
	public static String ESTIMATED_DATA = "estimated_data";
	public static String LIST_OF_TIME_FOR_REAL_DATA = "real_data_time";
	public static String LIST_OF_TIME_FOR_ESTIMATED_DATA = "estimated_data_time";
	public static String LIST_LISTEN_AGENT = "listen_agent";
	public static String LISTEN_AGENT_VAR_NAME = "topic_var_name";
	
}