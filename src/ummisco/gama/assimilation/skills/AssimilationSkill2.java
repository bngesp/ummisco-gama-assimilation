/**
* SimpleAssimilationSkills.java
* @author Bassirou NGOM
* create May 19, 2019
*/
package ummisco.gama.assimilation.skills;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import msi.gama.extensions.messaging.GamaMailbox;
import msi.gama.extensions.messaging.GamaMessage;
import msi.gama.metamodel.agent.IAgent;
import msi.gama.precompiler.GamlAnnotations.action;
import msi.gama.precompiler.GamlAnnotations.arg;
import msi.gama.precompiler.GamlAnnotations.doc;
import msi.gama.precompiler.GamlAnnotations.example;
import msi.gama.precompiler.GamlAnnotations.skill;
import msi.gama.precompiler.GamlAnnotations.variable;
import msi.gama.precompiler.GamlAnnotations.vars;
import msi.gama.precompiler.IConcept;
//import msi.gama.runtime.GAMA;
import msi.gama.runtime.IScope;
import msi.gama.runtime.exceptions.GamaRuntimeException;
import msi.gama.util.GamaMap;
import msi.gama.util.GamaMapFactory;
import msi.gaml.species.ISpecies;
import msi.gaml.statements.IStatement;
import msi.gaml.types.GamaType;
import msi.gaml.types.IType;
import msi.gaml.variables.IVariable;
import ummisco.gama.network.skills.INetworkSkill;
import ummisco.gama.network.skills.NetworkSkill;


@vars ({ @variable (
			name = IAssimilationSkill.REAL_DATA,
			type = IType.MAP,
			doc = @doc ("Net ID of the agent")),
		@variable (
				name = IAssimilationSkill.ESTIMATED_DATA,
				type = IType.MAP,
				doc = @doc ("Net ID of the agent")),
		@variable (
				name = IAssimilationSkill.LISTEN_AGENT_VAR_NAME,
				type = IType.MAP,
				doc = @doc ("Net ID of the agent"))
})

@skill(
		name = "assimiation_list",
		concept = {IAssimilationSkill.ASSIMILATION_CONCEPT,IConcept.NETWORK,IConcept.COMMUNICATION,IConcept.SKILL }
)
@doc("The "+IAssimilationSkill.ASSIMILATION_SKILL+" is an implementation of Assimilation meta-model. "
		+ "If you use this skill you must "
		+ "implements your own correction and estimation models")

public class AssimilationSkill2 extends NetworkSkill
{
	
	@SuppressWarnings({ "unchecked" })
	@action(
			name = IAssimilationSkill.LISTEN,
			args = {@arg(
					name=IAssimilationSkill.LISTEN_TO,
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"The Data variable"
					)),
					@arg(
						name= IAssimilationSkill.LISTEN_VAR,
						type = IType.STRING,
						//optional = true,
						doc=@doc(
							"the topic to listens"
						))},
			doc = @doc (
				value = "",
				returns = "void",
				examples = { @example (" do listen to:\"nomHum\" for:humidity") })
	)
	
	public void doListen(final IScope scope) throws GamaRuntimeException{
		//System.out.println("esk je suis nul ? "+this.getCurrentAgent(scope));
		final IAgent currentAgent = this.getCurrentAgent(scope);
		String	topicToListen = (String) scope.getArg(IAssimilationSkill.LISTEN_TO,IType.STRING);
		String varName = (String)scope.getArg(IAssimilationSkill.LISTEN_VAR,IType.STRING);
		
		if (!scope.getExperiment().hasAttribute(IAssimilationSkill.LIST_LISTEN_AGENT)) {
			this.doInit(scope);
			this.updateDataList(scope);
		}
		GamaMap<String, String> topic_to_var = (GamaMap<String, String>)currentAgent.getAttribute(IAssimilationSkill.LISTEN_AGENT_VAR_NAME);
		GamaMap<String, GamaMap<Double, Double>> data = (GamaMap<String, GamaMap<Double, Double>>)currentAgent.getAttribute(IAssimilationSkill.REAL_DATA);
		GamaMap<String, GamaMap<Double, Double>> estimated_data = (GamaMap<String, GamaMap<Double, Double>>)currentAgent.getAttribute(IAssimilationSkill.ESTIMATED_DATA);
		topic_to_var.put(topicToListen, varName);
		estimated_data.put(varName, GamaMapFactory.create());
		data.put(varName, GamaMapFactory.create());
		this.joinAGroup(scope,currentAgent,topicToListen);
		this.getListListenAgent(scope).add(currentAgent);
	}
	
	
	@action(
			name = "data_from_var",
			args = {@arg(
					name="var_name",
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"var name"
					))},
			doc = @doc (
				value = "",
				returns = "GamaMap",
				examples = { @example ("map m <- data_for_var(var_name)") })
	)
	public List<Double> getListRealDataFromVarName(final IScope scope) throws GamaRuntimeException{
		String varName = (String)scope.getArg("var_name",IType.STRING);
		GamaMap<Double, Double> data = this.getListRealData(scope.getAgent(), varName);
		List<Double> l = new ArrayList<>();
		data.forEach(
			(key, value) -> {
				l.add(value);
			}
			);
		
		return l;
	}
	
	
	@action(
			name = "time_from_var",
			args = {@arg(
					name="var_name",
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"var name"
					))},
			doc = @doc (
				value = "",
				returns = "GamaMap",
				examples = { @example ("map m <- data_for_var(var_name)") })
	)
	public List<Double> getLisTimeFromVarName(final IScope scope) throws GamaRuntimeException{
		String varName = (String)scope.getArg("var_name",IType.STRING);
		GamaMap<Double, Double> data = this.getListRealData(scope.getAgent(), varName);
		List<Double> l = new ArrayList<>();
		data.forEach(
			(key, value) -> {
				l.add(key);
			}
			);
		
		return l;
	}

	@action(
			name = "last_value",
			args = {@arg(
					name="var_name",
					type = IType.STRING,
					optional = true,
					doc=@doc(
						"var name"
					)),
					@arg(
							name= "value",
							type = IType.AVAILABLE_TYPES,
							//optional = true,
							doc=@doc(
								"the topic to listens"
							))},
			doc = @doc (
				value = "",
				returns = "GamaMap",
				examples = { @example ("map m <- data_for_var(var_name)") })
	)
	public IVariable getVarName(final IScope scope) throws GamaRuntimeException{
		String varName = (String)scope.getArg("var_name",IType.STRING);
		//IVariable varValue = (IVariable)scope.getArg("value",IType.AVAILABLE_TYPES);
		final IAgent agent = (IAgent)scope.getAgent();
		final ISpecies species = (ISpecies)agent.getSpecies();
		IVariable var = (IVariable) species.getVar(varName);
		//var.setValue(scope, varValue);
		var.setVal(scope, agent, scope.getArg("value",IType.AVAILABLE_TYPES));
		//return var.getType();
		//if(scope.getArg("value",IType.AVAILABLE_TYPES) instanceof GamaType<?>)
			//System.out.println("oui ceci est un type "+varValue.getType());
		return var;
	}
	
	
	@action(
			name = "list_var",
			args = {@arg(
					name="var_name",
					type = IType.STRING,
					optional = true,
					doc=@doc(
						"var name"
					))},
			doc = @doc (
				value = "",
				returns = "GamaMap",
				examples = { @example ("map m <- data_for_var(var_name)") })
	)
	public Collection<String> getAllVarName(final IScope scope) throws GamaRuntimeException{
		String varName = (String)scope.getArg("var_name",IType.STRING);
		//IVariable varValue = (IVariable)scope.getArg("value",IType.AVAILABLE_TYPES);
		final IAgent agent = (IAgent)scope.getAgent();
		final ISpecies species = (ISpecies)agent.getSpecies();
		
		return species.getVarNames();
	}
	
	@SuppressWarnings ({ "unchecked", "unused" })
	private GamaMap<Double, Double> getListRealData(final IAgent agent, final String varName){
		GamaMap<String, GamaMap<Double, Double>> list = (GamaMap<String, GamaMap<Double, Double>>)agent.getAttribute(IAssimilationSkill.REAL_DATA);
		return list.get(varName);
	}
	
	@SuppressWarnings("unused")
	private boolean hasCorrectionAction(final IScope scope) {
		return this.hasAssimilationAction(scope, IAssimilationSkill.CORRECTION_ACTION);
	}
	
	@SuppressWarnings("unused")
	private boolean hasEstimationAction(final IScope scope) {
		return this.hasAssimilationAction(scope, IAssimilationSkill.ESTIMATION_ACTION);
	}
	
	private boolean hasAssimilationAction(final IScope scope, final String actionName) {
		IAgent agent =  (IAgent)scope.getAgent();
		ISpecies species = (ISpecies)agent.getSpecies();
		List<String> names = species.getActionNames(scope);
		for (String string : names) {
			if(string.equals(actionName)) {
				return true;
			}
		}
		return false;
	}
	
	private void executeAction(final IScope scope, final IAgent agent, final String actionName) {
		//IAgent agent =  (IAgent)scope.getAgent();
		ISpecies species = (ISpecies)agent.getSpecies();
		IStatement actionStatement = species.getAction(actionName);
		
		
		System.out.println("actionName "+actionName+" pour le scope "+scope.getName());
		//scope.execute(actionStatement);
		scope.execute(actionStatement, agent, null);
	}
	
	private void updateDataList( final IScope scope) {
		scope.getSimulation().postEndAction(
				scope1 -> {
					System.out.println("hey je suis executer avec le scope "+ this.hasMoreMessage(scope));
					updateList(scope1);
					return null;
				});
		scope.getSimulation().postDisposeAction(
				scope1 -> {
			//closeAllConnection(scope1);
					doInit(scope1);
					return null;
				});
	}
	
	@SuppressWarnings ("unchecked")
	private void doInit(IScope scope) {
		
		ArrayList<IAgent>  listenAgent =  (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
		if(listenAgent == null) {
			listenAgent = this.initListenAgent(scope);
		}
		for (IAgent ag : listenAgent) {
			ag.setAttribute(IAssimilationSkill.ESTIMATED_DATA,  GamaMapFactory.create());
			ag.setAttribute(IAssimilationSkill.REAL_DATA,  GamaMapFactory.create());
			ag.setAttribute(IAssimilationSkill.LISTEN_AGENT_VAR_NAME, GamaMapFactory.create());
			//ag.setAttribute(IAssimilationSkill.LIST_OF_TIME_FOR_REAL_DATA,  new GamaMap<String, GamaMap<Double, Double>>());
		}
	}
	
	@SuppressWarnings ("unchecked")
	private ArrayList<IAgent> getListListenAgent(final IScope scope){
		//System.out.println("je suis appele :)");
		ArrayList<IAgent> list =  (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
		if(list == null) list = this.initListenAgent(scope);
		return list;
	}
	
	@SuppressWarnings ("unchecked")
	private ArrayList<IAgent> initListenAgent(final IScope scope){
		scope.getExperiment().setAttribute(IAssimilationSkill.LIST_LISTEN_AGENT, new ArrayList<IAgent>());
		return (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
	}
	
	@SuppressWarnings("unchecked")
	private void updateList(final IScope scope) {
		final ArrayList<IAgent> listAgent = this.getListListenAgent(scope);
		for (IAgent ag : listAgent) {
			final IScope localScope = ag.getScope();
			//final GamaMap<String, String> topic_to_var = (GamaMap<String, String>)ag.getAttribute(IAssimilationSkill.LISTEN_AGENT_VAR_NAME);
			if(!getMailbox(ag).isEmpty()) {
				System.out.println("moi j'ai un message :) " + ag.getName());
				GamaMessage msg = this.getAgentMessage(ag);
				if(msg!=null) {
					String data  = ((String)msg.getContents(localScope)).split(" ")[1];
					String varName = this.getVarNameFromTopic(ag, (String)msg.getReceivers());
					System.out.println("-----------------------------");
					System.out.println("-----------------------------");
					System.out.println("-- varName : "+varName+" ---------");
					System.out.println("-----------------------------");
					System.out.println("-----------------------------");
					GamaMap<String, GamaMap<Double, Double>> real_data_From_VarName = (GamaMap<String, GamaMap<Double, Double>>)ag.getAttribute(IAssimilationSkill.REAL_DATA);
					GamaMap<Double, Double> real_data = real_data_From_VarName.get(varName);
					real_data.put(scope.getClock().getTimeElapsedInSeconds(), Double.valueOf(data));
					IVariable var = ag.getSpecies().getVar(varName);
					if(var == null) System.out.println("la variable est nulle ");
					else var.setVal(localScope, ag, Double.valueOf(data));
					this.executeAction(ag.getScope(),ag, IAssimilationSkill.CORRECTION_ACTION);
					
				}
			}
		}

	}

	@SuppressWarnings("unchecked")
	private String getVarNameFromTopic(final IAgent ag, final String topic) {
		GamaMap<String, String> topic_to_var = (GamaMap<String, String>)ag.getAttribute(IAssimilationSkill.LISTEN_AGENT_VAR_NAME);
		String varName = topic_to_var.get(topic);
		return varName;
	}
	
	@SuppressWarnings("unchecked")
	protected ArrayList<GamaMessage> getAgentMessage2(final IAgent agent, final String topicToListen) {
		final GamaMailbox box = getMailbox(agent);
		ArrayList<GamaMessage> listMsg = new ArrayList<GamaMessage>();
		ArrayList<Integer> keyToRemove = new ArrayList<Integer>();
		if (!box.isEmpty()) {
			//int i=0;
			for (int j = 0; j < box.size(); j++) {
				if(box.get(j).getReceivers().equals(topicToListen)) {
					listMsg.add(box.get(j));
					keyToRemove.add(j);
				}
			}
			box.removeAll(keyToRemove);
//			GamaMessage msg = null;
//		
//			msg = box.get(0);
//			System.out.println("le recepteur du message est :"+msg.getReceivers());
//			ArrayList<String> groups = (ArrayList<String>) agent.getAttribute(INetworkSkill.NET_AGENT_GROUPS); 
//			 
//			if(groups.contains(msg.getReceivers())) {
//				System.out.println("je fais parti du groupe moi !!");
//			}
//			//if(msg.getReceivers().equals(agent.getScope().getArg(IAssimilationSkill.LISTEN_VAR, IType.STRING))) {
//				System.out.println("******************************");
//				System.out.println("Ce message est a toi boy :) "+box.length(agent.getScope()));
//				System.out.println("*******************************");
//				box.remove(0);
//				//return msg;
//			//}
		}
		return listMsg;
	}
	
	@SuppressWarnings("unchecked")
	protected GamaMessage getAgentMessage(final IAgent agent) {
		final GamaMailbox box = getMailbox(agent);
			GamaMessage msg = null;
			msg = box.get(0);
			System.out.println("le recepteur du message est :"+msg.getReceivers());
			ArrayList<String> groups = (ArrayList<String>) agent.getAttribute(INetworkSkill.NET_AGENT_GROUPS); 
	
			if(groups.contains(msg.getReceivers())) {
				System.out.println("je fais parti du groupe moi !!");
			}
			//if(msg.getReceivers().equals(agent.getScope().getArg(IAssimilationSkill.LISTEN_VAR, IType.STRING))) {
				System.out.println("******************************");
				System.out.println("Ce message est a toi boy :) "+box.length(agent.getScope()));
				System.out.println("*******************************");
				box.remove(0);
				return msg;
			//}
			//return null;
		
	}
	
	

}
