package ummisco.gama.assimilation.skills;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.regression.RegressionResults;

import msi.gama.extensions.messaging.GamaMessage;
import msi.gama.precompiler.GamlAnnotations.action;
import msi.gama.precompiler.GamlAnnotations.arg;
import msi.gama.precompiler.GamlAnnotations.doc;
import msi.gama.precompiler.GamlAnnotations.example;
import msi.gama.precompiler.GamlAnnotations.skill;
import msi.gama.precompiler.IConcept;
import msi.gama.runtime.IScope;
import msi.gama.runtime.exceptions.GamaRuntimeException;
import msi.gama.util.GamaRegression;
import msi.gaml.types.IType;
import ummisco.gama.assimilation.model.RegressionModel;
import ummisco.gama.dev.utils.DEBUG;
import ummisco.gama.network.skills.INetworkSkill;
import ummisco.gama.network.skills.NetworkSkill;

/**
* SimpleAssimilationSkills.java
* @author Bassirou NGOM
* create May 19, 2019
*/

@skill(
		name = IAssimilationSkill.SIMPLE_ASSIMILATION_SKILL,
		concept = { IAssimilationSkill.ASSIMILATION_CONCEPT, IConcept.NETWORK, IConcept.COMMUNICATION, IConcept.SKILL }
)
@doc("The "+IAssimilationSkill.SIMPLE_ASSIMILATION_SKILL+" is an implementation of Assimilation meta-model with minimal parameters")
public class SimpleAssimilationSkills extends NetworkSkill{
	//int index=0;
	RegressionModel regression= new RegressionModel();
	List<Float> data_sensor = new ArrayList<Float>();
	List<Float> time_list = new ArrayList<Float>();
	//GamaRegression regression;
	
	@action(
			name = "clear",
			doc = @doc (
				value = "",
				returns = "void",
				examples = { @example (" do clear()") })
	)
	public void clear(final IScope scope) throws GamaRuntimeException {
		this.regression = new RegressionModel();
		time_list = new ArrayList<Float>();
		data_sensor = new ArrayList<Float>();
	}
	@action(
			name = "data_connect",	
			args = {@arg(
					name="Tsimul",
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"The Tsimul variable"
					)),
					@arg(
					name="data",
					type = IType.STRING,
					doc=@doc(
						"The observation data"
					))},
			doc = @doc (
				value = "",
				returns = "Boolean",
				examples = { @example (" do data_connect Tsimul:time") })
	)
	public void doConnectoToData(final IScope scope) throws GamaRuntimeException {
		String	msg = (String) scope.getArg("data",IType.STRING);
		try {
			String value = msg.split(" ")[1];
			DEBUG.OUT("donnees recues: "+ value);
			this.data_sensor.add(Float.parseFloat(value));
			String tsimul = (String)scope.getArg("Tsimul", IType.STRING);
			this.regression.addObservation((Float.parseFloat(tsimul)), (Float.parseFloat(value)));
			DEBUG.OUT("cycle="+scope.getClock().getCycle());
			this.time_list.add((Float.parseFloat(tsimul)));
		}catch (Exception e) {
			DEBUG.OUT("erreur lors du casting");
		}
			//msg.
	}
	
	@action(
			name = "corrector",	
			doc = @doc (
				value = "",
				returns = "",
				examples = { @example ("") })
	)
	public void doCorrection(final IScope scope) throws GamaRuntimeException {
		
		
		this.regression = new RegressionModel(this.data_sensor,this.time_list); 
	}
	
	@action(
			name = "get_data_list",
			doc = @doc (
				value = "",
				returns = "list",
				examples = { @example (" list<float> list <- get_data_list") })
	)
	
	public List<Float> getData(final IScope scope) throws GamaRuntimeException{
		return this.data_sensor;
	}
	
	@action(
			name = "get_cycle_list",
			doc = @doc (
				value = "",
				returns = "list",
				examples = { @example (" list<float> list <- get_cycle_list") })
	)
	
	public List<Float> getCycleList(final IScope scope) throws GamaRuntimeException{
		return this.time_list;
	}
	
	@action(
			name = "param",	
			doc = @doc (
				value = "",
				returns = "Regression Parameters",
				examples = { @example ("") })
	)

	public List<Float> getRegressionParam(final IScope scope) throws GamaRuntimeException{
		double[] params= this.regression.getParam();
		List<Float> l = new ArrayList<Float>();
		l.add((float)params[0]);
		l.add((float)params[1]);
		return l;
	}
	
	@action(
			name = "estimated",	
			args = {@arg(
					name="Tsimul",
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"The Tsimul variable"
					))},
			doc = @doc (
				value = "",
				returns = "Boolean",
				examples = { @example (" do data_connect Tsimul:time") })
	)

	public double doPrediction(final IScope scope) throws GamaRuntimeException{
		String tsimul = (String)scope.getArg("Tsimul", IType.STRING);
		return this.regression.predict(Float.parseFloat(tsimul));
	}
}

//if(hasMoreMessage(scope1)) {
//System.out.println("moi j'ai un message :)");
//String msg = (String)this.fetchMessage(scope1).getContents(scope1);
//String a = msg.split(" ")[1];
//this.real_data.add(Double.valueOf(a));
//this.time_list.add(scope1.getClock().getTimeElapsedInSeconds());
//if(this.hasCorrectionAction(scope1)) {
//	System.out.print("cette especies contient une action correction ");
//	this.executeAction(scope1, IAssimilationSkill.CORRECTION_ACTION);
//}
//}


//@SuppressWarnings("unchecked")
//@action(
//		name = IAssimilationSkill.GET_REAL_DATA,
//		args = {@arg(
//				name=IAssimilationSkill.LISTEN_TO,
//				type = IType.STRING,
//				optional = true,
//				doc=@doc(
//					"The Data variable"
//				))},
//		doc = @doc (
//			value = "",
//			returns = "void",
//			examples = { @example (" do listen to:\"nomHum\" for:humidity") })
//)
//
//public List<Double> getRealData(final IScope scope) throws GamaRuntimeException{
//	IAgent ag = scope.getAgent();
//	return (ArrayList<Double>)ag.getAttribute(IAssimilationSkill.REAL_DATA);
//}
//
//@action(
//		name = "noms_action",
//		doc = @doc (
//			value = "",
//			returns = "void",
//			examples = { @example (" do listen to:\"nomHum\" for:humidity") })
//)
//
//
//public List<String> getListAction(final IScope scope) throws GamaRuntimeException{
//	IAgent agent =  (IAgent)scope.getAgent();
//	ISpecies species = (ISpecies)agent.getSpecies();	
//	if(this.hasEstimationAction(scope)) {
//		System.out.print("cette species contient une action estimation ");
//		this.executeAction(scope,scope.getAgent(), IAssimilationSkill.ESTIMATION_ACTION);
//	}
//	if(this.hasCorrectionAction(scope)) {
//		System.out.print("cette especies contient une action correction ");
//		this.executeAction(scope, scope.getAgent(), IAssimilationSkill.CORRECTION_ACTION);
//	}
//	return new ArrayList<String>(species.getVarNames());
//}