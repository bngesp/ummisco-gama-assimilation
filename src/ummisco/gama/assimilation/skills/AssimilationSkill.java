/**
* SimpleAssimilationSkills.java
* @author Bassirou NGOM
* create May 19, 2019
*/
package ummisco.gama.assimilation.skills;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import msi.gama.extensions.messaging.GamaMailbox;
import msi.gama.extensions.messaging.GamaMessage;
import msi.gama.metamodel.agent.IAgent;
import msi.gama.precompiler.GamlAnnotations.action;
import msi.gama.precompiler.GamlAnnotations.arg;
import msi.gama.precompiler.GamlAnnotations.doc;
import msi.gama.precompiler.GamlAnnotations.example;
import msi.gama.precompiler.GamlAnnotations.skill;
import msi.gama.precompiler.GamlAnnotations.variable;
import msi.gama.precompiler.GamlAnnotations.vars;
import msi.gama.precompiler.IConcept;
import msi.gama.runtime.GAMA;
import msi.gama.runtime.IScope;
import msi.gama.runtime.exceptions.GamaRuntimeException;
import msi.gama.util.GamaMap;
import msi.gaml.species.ISpecies;
import msi.gaml.statements.IStatement;
import msi.gaml.types.IType;
import ummisco.gama.network.skills.INetworkSkill;
import ummisco.gama.network.skills.NetworkSkill;


@vars ({ @variable (
			name = IAssimilationSkill.REAL_DATA,
			type = IType.LIST,
			doc = @doc ("Net ID of the agent")),
		@variable (
				name = IAssimilationSkill.ESTIMATED_DATA,
				type = IType.LIST,
				doc = @doc ("Net ID of the agent"))
})

@skill(
		name = IAssimilationSkill.ASSIMILATION_SKILL,
		concept = {IAssimilationSkill.ASSIMILATION_CONCEPT,IConcept.NETWORK,IConcept.COMMUNICATION,IConcept.SKILL }
)
@doc("The "+IAssimilationSkill.ASSIMILATION_SKILL+" is an implementation of Assimilation meta-model. "
		+ "If you use this skill you must "
		+ "implements your own correction and estimation models")

public class AssimilationSkill extends NetworkSkill
{
	
	@SuppressWarnings("unused")
	@action(
			name = IAssimilationSkill.LISTEN,
			args = {@arg(
					name=IAssimilationSkill.LISTEN_TO,
					type = IType.STRING,
					//optional = true,
					doc=@doc(
						"The Data variable"
					)),
					@arg(
						name= IAssimilationSkill.LISTEN_VAR,
						type = IType.STRING,
						//optional = true,
						doc=@doc(
							"the topic to listens"
						))},
			doc = @doc (
				value = "",
				returns = "void",
				examples = { @example (" do listen to:\"nomHum\" for:humidity") })
	)
	
	public void doListen(final IScope scope) throws GamaRuntimeException{
		System.out.println("esk je suis nul ? "+this.getCurrentAgent(scope));
		final IAgent currentAgent = this.getCurrentAgent(scope);
		String	topicToListen = (String) scope.getArg(IAssimilationSkill.LISTEN_TO,IType.STRING);
		String varName = (String)scope.getArg(IAssimilationSkill.LISTEN_VAR,IType.STRING);
		
		if (!scope.getExperiment().hasAttribute(IAssimilationSkill.LIST_LISTEN_AGENT)) {
			this.doInit(scope);
			this.updateDataList(scope);
		}
		this.joinAGroup(scope,currentAgent,topicToListen);
		this.getListListenAgent(scope).add(currentAgent);
	}
	
	
	
	private boolean hasCorrectionAction(final IScope scope) {
		return this.hasAssimilationAction(scope, IAssimilationSkill.CORRECTION_ACTION);
	}
	
	private boolean hasEstimationAction(final IScope scope) {
		return this.hasAssimilationAction(scope, IAssimilationSkill.ESTIMATION_ACTION);
	}
	
	private boolean hasAssimilationAction(final IScope scope, final String actionName) {
		IAgent agent =  (IAgent)scope.getAgent();
		ISpecies species = (ISpecies)agent.getSpecies();
		List<String> names = species.getActionNames(scope);
		for (String string : names) {
			if(string.equals(actionName)) {
				return true;
			}
		}
		return false;
	}
	
	private void executeAction(final IScope scope, final IAgent agent, final String actionName) {
		//IAgent agent =  (IAgent)scope.getAgent();
		ISpecies species = (ISpecies)agent.getSpecies();
		IStatement actionStatement = species.getAction(actionName);
		
		
		System.out.println("actionName "+actionName+" pour le scope "+scope.getName());
		//scope.execute(actionStatement);
		scope.execute(actionStatement, agent, null);
	}
	
	private void updateDataList( final IScope scope) {
		scope.getSimulation().postEndAction(
				scope1 -> {
					System.out.println("hey je suis executer avec le scope "+ this.hasMoreMessage(scope));
					updateList(scope1);
					return null;
				});
		scope.getSimulation().postDisposeAction(scope1 -> {
			//closeAllConnection(scope1);
			doInit(scope1);
			return null;
		});
	}
	
	@SuppressWarnings ("unchecked")
	private void doInit(IScope scope) {
		
		ArrayList<IAgent>  listenAgent =  (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
		if(listenAgent == null) {
			listenAgent = this.initListenAgent(scope);
		}
		for (IAgent ag : listenAgent) {
			ag.setAttribute(IAssimilationSkill.ESTIMATED_DATA, new ArrayList<Double> ());
			ag.setAttribute(IAssimilationSkill.REAL_DATA,  new ArrayList<Double> ());
			ag.setAttribute(IAssimilationSkill.LIST_OF_TIME_FOR_REAL_DATA,  new ArrayList<Double>());
		}
	}
	
	@SuppressWarnings ("unchecked")
	private ArrayList<IAgent> getListListenAgent(final IScope scope){
		//System.out.println("je suis appele :)");
		ArrayList<IAgent> list =  (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
		if(list == null) list = this.initListenAgent(scope);
		return list;
	}
	
	@SuppressWarnings ("unchecked")
	private ArrayList<IAgent> initListenAgent(final IScope scope){
		scope.getExperiment().setAttribute(IAssimilationSkill.LIST_LISTEN_AGENT, new ArrayList<IAgent>());
		return (ArrayList<IAgent>) scope.getExperiment().getAttribute(IAssimilationSkill.LIST_LISTEN_AGENT);
	}
	
	@SuppressWarnings("unchecked")
	private void updateList(final IScope scope) {
		final ArrayList<IAgent> listAgent = this.getListListenAgent(scope);
		for (IAgent ag : listAgent) {
			final IScope localScope = ag.getScope();
			if(!getMailbox(ag).isEmpty()) {
				System.out.println("moi j'ai un message :)" + ag.getName());
				GamaMessage msg = this.getAgentMessage(ag);
				if(msg!=null) {
					String data  = ((String)msg.getContents(localScope)).split(" ")[1];
					ArrayList<Double> real_data = (ArrayList<Double>)ag.getAttribute(IAssimilationSkill.REAL_DATA);
					ArrayList<Double> list_time = (ArrayList<Double>)ag.getAttribute(IAssimilationSkill.LIST_OF_TIME_FOR_REAL_DATA);
					real_data.add(Double.valueOf(data));
					list_time.add(scope.getClock().getTimeElapsedInSeconds());
					//Appel a la method Correction
					this.executeAction(ag.getScope(),ag, IAssimilationSkill.CORRECTION_ACTION);
				}
			}
		}

	}
	
	protected GamaMessage getAgentMessage(final IAgent agent) {
		final GamaMailbox box = getMailbox(agent);
		GamaMessage msg = null;
		if (!box.isEmpty()) {
			msg = box.get(0);
			System.out.println("le recepteur du message est :"+msg.getReceivers());
			ArrayList<String> groups = (ArrayList<String>) agent.getAttribute(INetworkSkill.NET_AGENT_GROUPS); 
			 
			if(groups.contains(msg.getReceivers())) {
				System.out.println("je fais parti du groupe moi !!");
			}
			//if(msg.getReceivers().equals(agent.getScope().getArg(IAssimilationSkill.LISTEN_VAR, IType.STRING))) {
				System.out.println("******************************");
				System.out.println("Ce message est a toi boy :) "+box.length(agent.getScope()));
				System.out.println("*******************************");
				box.remove(0);
				return msg;
			//}
		}
		return msg;
	}
}
/*
 * 	private void updateList(final IScope scope) {
		final ArrayList<IAgent> listAgent = this.getListListenAgent(scope);
		for (IAgent ag : listAgent) {
			final IScope localScope = ag.getScope();
			final GamaMap<String, String> topic_to_var = (GamaMap<String, String>)ag.getAttribute(IAssimilationSkill.LISTEN_AGENT_VAR_NAME);
			if(!getMailbox(ag).isEmpty()) {
				System.out.println("moi j'ai un message :)" + ag.getName());
				topic_to_var.forEach(
						(topic, var) ->{
							ArrayList<GamaMessage> msg = getAgentMessage(ag, topic);
							if(msg.size()>0) {
								msg.forEach(
										message -> {
											String data  = ((String)message.getContents(localScope)).split(" ")[1];
											String varName = this.getVarNameFromTopic(ag, (String)message.getReceivers());
											GamaMap<String, GamaMap<Double, Double>> real_data_From_VarName = (GamaMap<String, GamaMap<Double, Double>>)ag.getAttribute(IAssimilationSkill.REAL_DATA);
											GamaMap<Double, Double> real_data = real_data_From_VarName.get(varName);
											real_data.put(scope.getClock().getTimeElapsedInSeconds(), Double.valueOf(data));
											this.executeAction(ag.getScope(),ag, IAssimilationSkill.CORRECTION_ACTION);
										}
								);
							}
						}
				);
				
			}
		}

	}
 * 
 */
